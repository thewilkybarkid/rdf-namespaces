import { TripleSubject } from 'tripledoc';

export function getTs(
  entity: TripleSubject,
  namespace: string,
  entityTypes: {[alias: string]: string},
): string {
  const entityType = Object.entries(entityTypes)
    .find(([_alias, type]) => type === entity.getNodeRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'));
  const typeAlias = entityType ? entityType[0] : 'string';
  const comment = entity.getLiteral('http://www.w3.org/2000/01/rdf-schema#comment');
  const formattedComment = (typeof comment === 'string') ? comment.replace(/\n/g, '\n * ') : comment;

  return `
/**
 * ${entity.getLiteral('http://www.w3.org/2000/01/rdf-schema#label') || ''}
 * 
 * ${formattedComment || ''}
 *
 * ${entity.asNodeRef()}
 */
export const ${entity.asNodeRef().substring(namespace.length)}: ${typeAlias} = '${entity.asNodeRef()}';
`;
}
