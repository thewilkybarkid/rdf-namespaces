import { fetchDocument, TripleSubject } from 'tripledoc';
import { getTs } from './getTs';

export type Mirrors = {[namespace: string]: string};
export async function generateNamespaceTs(
  namespace: string,
  options = {
    mirrors: {} as Mirrors
  },
) {
  const entityTypes = { 
    Property: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Property',
    Class: 'http://www.w3.org/2000/01/rdf-schema#Class',
    Datatype: 'http://www.w3.org/2000/01/rdf-schema#Datatype',
    OwlClass: 'http://www.w3.org/2002/07/owl#Class',
    OwlObjectProperty: 'http://www.w3.org/2002/07/owl#ObjectProperty',
    OwlDatatypeProperty: 'http://www.w3.org/2002/07/owl#DatatypeProperty',
  };

  const schemaLocation = options.mirrors[namespace] || namespace;
  const schemaDoc = await fetchDocument(schemaLocation);

  const entities = Object.values(entityTypes).reduce<TripleSubject[]>(
    (entitiesSoFar, entityType) => {
      const entitiesOfThisType = schemaDoc.getSubjectsOfType(entityType);
      const newEntitiesOfThisType = entitiesOfThisType.filter((entityOfThisType) => {
        // Only include this entity if it is not present in the list yet:
        return entitiesSoFar
          .findIndex((entity) => entity.asNodeRef() === entityOfThisType.asNodeRef()) === -1;
      });
      
      return entitiesSoFar.concat(newEntitiesOfThisType);
    },
    [],
  );
  const typeAliases = Object.keys(entityTypes).map(alias => `type ${alias} = string;`).join('\n');
  const entityTs = entities
    .filter(entity => {
      const entityName = entity.asNodeRef().substring(namespace.length);
      return (
        // Only include names that are valid Javascript identifiers (i.e. alphanumeric characters,
        // underscores and dollar signs allowed, but shouldn't start with a digit)...
        /^[A-Za-z_\$](\w|\$)*$/.test(entityName) &&
        // ...are not one of the reserved keywords...
        !reservedKeywords.includes(entityName) &&
        // ...and are actually in this namespace:
        entity.asNodeRef().substring(0, namespace.length) === namespace
      );
    })
    .map(entity => getTs(entity, namespace, entityTypes)).join('');
  const typescript = typeAliases + '\n' + entityTs;
  return typescript;
}

// Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords
const reservedKeywords = [
  'break',
  'case',
  'catch',
  'class',
  'const',
  'continue',
  'debugger',
  'default',
  'delete',
  'do',
  'else',
  'export',
  'extends',
  'finally',
  'for',
  'function',
  'if',
  'import',
  'in',
  'instanceof',
  'new',
  'return',
  'super',
  'switch',
  'this',
  'throw',
  'try',
  'typeof',
  'var',
  'void',
  'while',
  'with',
  'yield',
];
