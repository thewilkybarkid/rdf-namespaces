# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.0] - 2019-10-25

### New features

- The Simple Knowledge Organization System (SKOS) vocabulary is now also included.

## [1.4.0] - 2019-10-01

### New features

- OWL Datatype Properties are now also included.

## [1.3.1] - 2019-09-18

### Bugs fixed

- Some properties were exported that were actually part of a different namespace than the one they were listed under.

## [1.3.0] - 2019-09-08

### New features

- A version of the code using ES Modules instead of CommonJS modules is now also published. Find it
  under the `module` key in `package.json`.

## [1.2.0] - 2019-09-04

### New features

- The full URL that will be imported is now included in each export's TSDoc.

## [1.1.2] - 2019-08-28

### Bugs fixed

- README listing an incorrect package name.

## [1.1.1] - 2019-08-21

### Bugs fixed

- tripledoc is not actually needed for consumers of this library, yet was listed as a regular dependency. It is now a devDependency.

## [1.1.0] - 2019-08-21

### New features

- Schema.org is now exported as well

## [1.0.0] - 2019-08-21

### New features

- First release!
